package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ex4spring{
    public static void main(String[] args) {
        SpringApplication.run(ex4spring.class, args);
    }
}