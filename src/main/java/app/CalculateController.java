package app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculateController {
    @RequestMapping("/square")
    public int calculateSquare(@RequestParam("num") int num){
        return num*num;
    }

    @RequestMapping("/sum")
    public int calculateSum(@RequestParam ("a") int a, @RequestParam ("b") int b){
        return a+b;
    }
}
